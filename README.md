# Introduction
Pathways is an **user-friendly entity paths exploration system** capable of showing the complete set of connections between two types of entities, such as people, places, organizations, emails, etc.

Pathways has been developed on top of the [Abstra project](https://gitlab.inria.fr/cedar/abstra). The repository is organized as follows:
 - `core`: the core of the system;
 - `datasets`: the datasets used in the experiments section;
 - `experiments`: the scripts used for the experiments section;
 - `scripts`: the Python scripts used fo the data preprocessing and named entity extraction.
<!--- `gui`: a Web-based interactive interface to create and play with abstractions and to explore the dataset;-->
 
Authors: Nelly Barret (Institut Polytechnique de Paris and Inria), Antoine Gauquier (IMT Nord-Europe), Jia Jean Law (Ecole Polytechnique), Ioana Manolescu (Institut Polytechnique de Paris and Inria).  
Please visit [our project website](https://team.inria.fr/cedar/projects/pathways/) for more details about the concepts and algorithms.

# Pre-requisites

You must have installed:
- [Postgres](https://www.postgresql.org/download/) (at least v9.6). A PostgreSQL server must be running locally, and you must have access to an account with the ability to create users. By default, the username is `pathways_user`, therefore this user must be created beforehand with `CREATE DATABASE` privileges.
- [Java](https://www.oracle.com/fr/java/technologies/javase/jdk11-archive-downloads.html) (v11 is recommended)
- [Python](https://www.python.org/downloads/) (v3.6 or v3.7 are recommended). Please note that your python3 installation should point to Python3.6 or Python3.7.
- [Dot](https://graphviz.org/download/)
<!-- - [Tomcat](https://tomcat.apache.org/download-90.cgi) (at least v9)-->

# Getting started

## Clone the repository
   
Clone the repository with `git clone https://gitlab.inria.fr/nbarret/abstraction-work.git`
     
## Run the `configure` script

From the `pathways` directory, run the `configure` script as follows:
- **For Linux and MacOS users**, run `./configure.sh` <!-- -t path/to/deployed/tomcat/gui --> 
- **For Windows users**, run `configure.bat` <!-- path/to/deployed/tomcat/gui --> 

<!--where `path/to/abstra/dir` is a path to a user-specified directory where Abstra will store its resources and write auxiliary files, e.g. `./abstra-working-dir/`, and `path/to/deployed/tomcat/gui` is the path of the Tomcat deployed GUI, e.g. `/Applications/apache-tomcat-9.0.41/webapps/gui` for MacOS users. If you have not deployed the GUI yet, simply set `#` for the `-t` setting (you may need to escape the sharp on some systems).-->

**This will**:
- Create the configuration file `local.settings` under `core/src/main/resources`. This is the main configuration file.
- Copy all the resources that Pathways needs in the folder `pathways-working-dir`.

If needed, manually update the `RDBMS_user` and `RDBMS_password` properties with your postgres user and password in the `local.settings` file present under `core/src/main/resources/`.
   
# Running an abstraction with the command-line

We provide a JAR of PathWays which encompasses the core of the system. <!-- as well as the GUI. -->

You can run the abstraction of a dataset with the default parameters by running **from the `pathways` directory** a command such as: 

`java -jar core/pathways-core-full-*.jar -i path/to/dataset -DRDBMS_DBName=pathways_db`

This will load and abstract the input specified with the option `-i` into the database labelled `pathways_db` with the default Pathways parameters.

You can tune several every abstraction parameter as follows:
- `-el` tunes the left entity type. Choose among `ENTITY_PERSON`, `ENTITY_LOCATION`, `ENTITY_ORGANIZATION`, `EMAIL`, `HASHTAG`, `DATE` or `MENTION`. Default is null.
- `-er` tunes the right entity type. Same values as for `-el`. Default is null.
- `-pd` tunes the accepted path directionalities. Possible values are `UNIDIRECTIONAL`, `ROOT_SHARED`, `SINK_SHARED` and `GENERAL`; you can specify one or mor path directionalities. By default, all path directionalities are accepted.
- `-ns` tunes whether enumerated paths are not evaluated. Values are: `true` or `false`. Default is `false`. 
- `-log` tunes the logger level, s.t. the value is DEBUG or INFO (default is INFO).

We also provide parameters to tune the general pipeline:
- `-n`: do not reset the ingested data and start to run the Pathways pipeline on already-loaded data.
- `-cg`: start from the collection graph (stored on disk) to avoid data loading, normalization and collection graph building. To be combined with `-n`.

After running the complete pipeline, you can find the final result in the PathWays working directory. You have in hand a JSON file containing, for each collection path, all the enumerated data paths.

<!--
# Running an abstraction with the GUI

## Deploy the GUI 

1. Copy the `gui.war` archive in the Tomcat webapps folder (e.g. `/Applications/apache-tomcat-9.0.41/webapps/`) and unzip it there.
2. Copy your `local.settings` under `core/src/main/resources/` in `gui/WEB-INF/` in the deployed GUI folder.
3. Open a Web browser at the following address: `localhost:8080/gui/index.jsp`.


## GUI features

The main page offers three features:
- Create abstractions
- Read existing abstractions
- Visualize the graphs created for the abstractions

Help is provided for each feature in the interface with the "Help" buttons.
-->
