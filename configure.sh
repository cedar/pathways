#!/bin/bash

usage() {
  echo "Usage: ${0} "
  exit 0
}

# no slash at the end, it will be happen when adding folders
PATHWAYS_DIR=$(pwd)/pathways-working-dir

if [[ -z ${PATHWAYS_DIR} ]] # || [ -z ${APP_DIR} ]
then
  usage
else
  echo "PATHWAYS_DIR is :'$PATHWAYS_DIR'"
  # echo "APP_DIR is :'$APP_DIR'"
fi

ROOT=core/lib
CACHE_DIR=cache
TMP_DIR=tmp
TT_DIR=treetagger
MODELS_DIR=models
CMD_DIR=cmd
LIB_DIR=lib
SCRIPTS_DIR=scripts
HEIDELTIME_DIR=heideltime
CLASSIFICATION_DIR=classification
REPORTING_DIR=reporting
RDFQUOTIENT_DIR=rdfquotient

# get OS to get the relevant executables for treetagger
TT_BIN=""
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  # Linux OS
  TT_BIN=binLinux;
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # Mac OSX
  TT_BIN=binMacos
else
  # by default, we will try with Linux
  TT_BIN=binLinux
fi

ABSTRA_TT=${ROOT}/${TT_DIR}
ABSTRA_TT_BIN=${ABSTRA_TT}/${TT_BIN}
ABSTRA_TT_CMD=${ABSTRA_TT}/${CMD_DIR}
ABSTRA_TT_LIB=${ABSTRA_TT}/${LIB_DIR}
ABSTRA_TT_MODELS=${ABSTRA_TT}/${MODELS_DIR}
ABSTRA_HEIDELTIME=${ROOT}/${HEIDELTIME_DIR}
ABSTRA_SCRIPTS=${SCRIPTS_DIR}
ABSTRA_CLASSIFICATION=${ROOT}/${CLASSIFICATION_DIR}
ABSTRA_REPORTING=${ROOT}/${REPORTING_DIR}
ABSTRA_RDFQUOTIENT=${ROOT}/${RDFQUOTIENT_DIR}

LOCAL_CACHE=${PATHWAYS_DIR}/${CACHE_DIR}
LOCAL_TMP=${PATHWAYS_DIR}/${TMP_DIR}
LOCAL_TT=${PATHWAYS_DIR}/${TT_DIR}
LOCAL_TT_BIN=${LOCAL_TT}/bin
LOCAL_TT_CMD=${LOCAL_TT}/${CMD_DIR}
LOCAL_TT_LIB=${LOCAL_TT}/${LIB_DIR}
LOCAL_TT_MODELS=${LOCAL_TT}/${MODELS_DIR}
LOCAL_HEIDELTIME=${PATHWAYS_DIR}/${HEIDELTIME_DIR}
LOCAL_SCRIPTS=${PATHWAYS_DIR}/${SCRIPTS_DIR}
LOCAL_CLASSIFICATION=${PATHWAYS_DIR}/${CLASSIFICATION_DIR}
LOCAL_REPORTING=${PATHWAYS_DIR}/${REPORTING_DIR}
LOCAL_RDFQUOTIENT=${PATHWAYS_DIR}/${RDFQUOTIENT_DIR}
LOCAL_PYTHON_ENV=${PATHWAYS_DIR}/abstra_env
LOCAL_SETTINGS=${PATHWAYS_DIR}/local.settings


# create folders
mkdir -p $PATHWAYS_DIR
mkdir -p $LOCAL_CACHE
mkdir -p $LOCAL_TMP
mkdir -p $LOCAL_TT
mkdir -p $LOCAL_TT_BIN
mkdir -p $LOCAL_TT_CMD
mkdir -p $LOCAL_TT_LIB
mkdir -p $LOCAL_TT_MODELS
mkdir -p $LOCAL_HEIDELTIME
mkdir -p $LOCAL_SCRIPTS
mkdir -p $LOCAL_CLASSIFICATION
mkdir -p $LOCAL_REPORTING
mkdir -p $LOCAL_RDFQUOTIENT

# copy necessary files
cp -R ${ABSTRA_TT_BIN}/. ${LOCAL_TT_BIN}/.
cp -R ${ABSTRA_TT_CMD}/. ${LOCAL_TT_CMD}/.
cp -R ${ABSTRA_TT_LIB}/. ${LOCAL_TT_LIB}/.
cp -R ${ABSTRA_TT_MODELS}/. ${LOCAL_TT_MODELS}/.
cp -R ${ABSTRA_SCRIPTS}/. ${LOCAL_SCRIPTS}/.
cp -R ${ABSTRA_HEIDELTIME}/. ${LOCAL_HEIDELTIME}/.
cp -R ${ABSTRA_CLASSIFICATION}/. ${LOCAL_CLASSIFICATION}/.
cp -R ${ABSTRA_REPORTING}/. ${LOCAL_REPORTING}/.
cp -R ${ABSTRA_RDFQUOTIENT}/. ${LOCAL_RDFQUOTIENT}/.

cp core/src/main/resources/parameter.settings $PATHWAYS_DIR/local.settings


# configure Python venv
# Mac OSX or Linux OS
python3 -m venv ${LOCAL_PYTHON_ENV}
source ${LOCAL_PYTHON_ENV}/bin/activate
python3 -m pip install --upgrade pip
pip3 install -r requirements.txt
deactivate


# add to local.settings the  absolute path to models
printf "\n\n\n#### GENERATED PARAMETERS\n\n" >> ${LOCAL_SETTINGS}

printf "# Temporary directory for Abstra\n" >> ${LOCAL_SETTINGS}
printf "temp_dir=${LOCAL_TMP}\n\n" >> ${LOCAL_SETTINGS}

#printf "# Tomcat directory for Abstra GUI\n" >> ${LOCAL_SETTINGS}
#printf "tomcat_dir=${APP_DIR}\n\n" >> ${LOCAL_SETTINGS}

printf "# Abstra cache location\n" >> ${LOCAL_SETTINGS}
printf "cache_location=${LOCAL_CACHE}\n\n" >> ${LOCAL_SETTINGS}

printf "# Python location\n" >> ${LOCAL_SETTINGS}
printf "python_path=${LOCAL_PYTHON_ENV}/bin/python\n\n" >> ${LOCAL_SETTINGS}
printf "# Path to python scripts (FLAIR extraction)\n" >> ${LOCAL_SETTINGS}
printf "python_script_location=${LOCAL_SCRIPTS}\n\n" >> ${LOCAL_SETTINGS}
printf "data_processing_script_location=${LOCAL_SCRIPTS}/data_preprocessing\n\n" >> ${LOCAL_SETTINGS}
printf "# Treetagger location\n" >> ${LOCAL_SETTINGS}
printf "treetagger_home=${LOCAL_TT}\n\n" >> ${LOCAL_SETTINGS}
printf "# Stanford models location\n" >> ${LOCAL_SETTINGS}
printf "stanford_models=${LOCAL_TT_MODELS}\n\n" >> ${LOCAL_SETTINGS}
printf "# Configuration file used by HeidelTime (date extractor)\n" >> ${LOCAL_SETTINGS}
printf "config_heideltime=${LOCAL_HEIDELTIME}/config-heideltime.props\n\n" >> ${LOCAL_SETTINGS}

printf "# Word2Vec model location (used for classification during abstraction)\n" >> ${LOCAL_SETTINGS}
printf "word_embedding_model_path=${LOCAL_CLASSIFICATION}/word2vec.bin\n" >> ${LOCAL_SETTINGS}
printf "stop_words_english=${LOCAL_CLASSIFICATION}/stop_words_english.txt\n" >> ${LOCAL_SETTINGS}
printf "stop_words_french=${LOCAL_CLASSIFICATION}/stop_words_french.txt\n\n" >> ${LOCAL_SETTINGS}
printf "# set of manually-defined semantic properties\n" >> ${LOCAL_SETTINGS}
printf "set_of_semantic_properties_manual=${LOCAL_CLASSIFICATION}/set-of-semantic-properties-manual.json\n\n" >> ${LOCAL_SETTINGS}
printf "# set of semantic properties extended with Yago and DBPedia triples\n" >> ${LOCAL_SETTINGS}
printf "set_of_semantic_properties_extended=${LOCAL_CLASSIFICATION}/set-of-semantic-properties-extended.json\n\n" >> ${LOCAL_SETTINGS}
printf "# set of classes (domain/range) extracted from GitTables semantic properties\n" >> ${LOCAL_SETTINGS}
printf "gitTables_classes=${LOCAL_CLASSIFICATION}/GitTables-classes.txt\n\n" >> ${LOCAL_SETTINGS}
printf "# set of semantic properties provided by GitTables\n" >> ${LOCAL_SETTINGS}
printf "gitTables_semantic_properties=${LOCAL_CLASSIFICATION}/GitTables-semantic-properties.json\n\n" >> ${LOCAL_SETTINGS}
printf "# class hierarchy for Schema.org classes\n" >> ${LOCAL_SETTINGS}
printf "schemaorg_class_hierarchy=${LOCAL_CLASSIFICATION}/class-hierarchy-schemaorg-cleaned.json\n\n" >> ${LOCAL_SETTINGS}
printf "# synonyms for name,designation,denomination file\n" >> ${LOCAL_SETTINGS}
printf "synonyms_denomination_file=${LOCAL_CLASSIFICATION}/words-related-to-denomination.txt\n\n" >> ${LOCAL_SETTINGS}
printf "# mapping between our extracted entity types and Schema.org and DBPedia classes (e.g. Person is same as dbpedia:Person)\n" >> ${LOCAL_SETTINGS}
printf "mapping_classes_to_categories=${LOCAL_CLASSIFICATION}/mapping-classes-to-categories.json\n\n" >> ${LOCAL_SETTINGS}


# add to config-heideltime.props the absolute path to TreeTagger
printf "\n" >> ${LOCAL_HEIDELTIME}/config-heideltime.props
printf "#### GENERATED PARAMETERS\n" >> ${LOCAL_HEIDELTIME}/config-heideltime.props
printf "treeTaggerHome=${LOCAL_TT}\n\n" >> ${LOCAL_HEIDELTIME}/config-heideltime.props

cp ${LOCAL_SETTINGS} core/src/main/resources
cp ${LOCAL_SETTINGS} gui/WebContent/WEB-INF