import re
import json
import io
import sys

def map_entity(type):
	"""Map the entity type found by the extractor to the set of pre-defined entities"""
	if type.lower() == 'per':
		return 'person'
	if type.lower() == 'org':
		return 'organization'
	if type.lower() == 'loc':
		return 'location'
	return 'misc'

def normalize(value, offset):
	"""Normalize the value and modify the offset correspondingly"""
	prog = re.compile("^((le|la|les|l'|Le|La|Les|L'|M\\.|Mme\\.|Mlle\\.|Dr\\.)\\s?)")
	result = prog.match(value)
	if result:
		value = value.replace(result.group(1), '')
		offset += len(result.group(1))
	return value, offset

#######################################
### check the required parameter(s) ###
#######################################
if len(sys.argv) != 4:
	print("Usage: python spacy_script.py <input> <output> <lang>")
	sys.exit(-1)

######################
### get the params ###
######################
import spacy
in_file = sys.argv[1]
out_file = sys.argv[2]
lang = sys.argv[3]

##########################
### check the language ###
##########################
if lang == 'fr':
	import fr_core_news_md as model
elif lang == 'en':
	import en_core_web_sm as model
else:
	sys.exit(-1)

nlp = model.load() # load the extracting model

####################################
### read the input text as utf-8 ###
####################################
with io.open(in_file,'r',encoding='utf-8') as f:
  text = f.read()
doc = nlp(str(text))

########################
### start extracting ###
########################
r = ''
entities = []
for ent in doc.ents:
	value, offset = normalize(ent.text, ent.start_char)
	occ = {
		"value": value,
		"commonName": value,
		"confidence": 0.0,
		"offset": offset,
		"length": ent.end_char - offset + 1,
		"docID": ""
	}

	matched = False
	ent_type = map_entity(ent.label_)
	for e in entities:
		if e["type"].lower() == ent_type:
			e["occurrences"].append(occ)
			matched = True
			break

	if not matched:
		e = {
			"type": ent_type,
			"occurrences": [occ]
		}
		entities.append(e)
	
	r += ent.text + ' - ' + str(ent.start_char) + ' --> ' + ent.label_ + '\n'

################################
### write the object to json ###
################################
js_result = json.dumps({
	"entities": entities
}, indent=2)

################################
### write the result in file ###
################################
with io.open(out_file,'w',encoding='utf-8') as f:
  f.write(js_result)

############################
### return the exit code ###
############################
print('Terminating...')
sys.exit(0)