import wikipedia
from googletrans import Translator
import pandas as pd
# query = 'Rafaela Aponte'
# answer = []
# N=0
# for url in search(query, stop=10, lang='fr'):
#     if N==10:
#         break
#     else :
#         a = google_scrape(url)
#         tab = a.replace("\n",".").split(".")
#         for sentence in tab :
#             if("Rafaela Aponte") in sentence :
#                 answer.append(sentence)
#                 N+=1
# print(answer)

def find_sentences(person) :
    N=1
    answer=[]
    franswer=[]
    translator = Translator()

    wikipedia.set_lang("en")
    search = wikipedia.search(person)
    for sugg in search :
        if "(disambiguation)" in sugg :
            search.remove(sugg)
    for sugg in search :
            if(N>10):
                break
            try:
                page = wikipedia.page(sugg)
            except wikipedia.exceptions.DisambiguationError as e:
                continue
            except wikipedia.exceptions.PageError as e:
                continue
            text = page.content
            splitted = text.replace('\n','.').split(".")
            for sentence in splitted:
                if N>10 :
                    break
                else:
                    if person in sentence:
                        if((len(sentence)>len(person)+10) and sentence not in answer):
                            answer.append(sentence)
                            N+=1
    translations = translator.translate(answer,src='en',dest='fr')
    for translation in translations:
        franswer.append(translation.text)
    return(franswer)

sentences = pd.read_csv('sentences.csv', sep='\t', encoding='utf-8')

i=0
for person in sentences["ceo"] :
    if pd.isna(sentences.iloc[i,1]):
        print(person)
        sentences.iloc[i,1]=find_sentences(person)
        print(sentences.iloc[i,1])
        sentences.to_csv('sentences.csv', sep='\t', encoding='utf-8',index=False)
        print(len(sentences.iloc[0:i,1])/len(sentences["ceo"])*100)
    i+=1

sentences.to_csv('sentences.csv', sep='\t', encoding='utf-8',index=False)

df = pd.read_csv('companies-2000.csv',sep=',')
df_short = df[['name','ceo']]
df_short.to_csv('companies-4.txt',sep='\t',index=False)
