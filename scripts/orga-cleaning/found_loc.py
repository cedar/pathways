""" Toutes les étapes ci-dessous concernent les étapes d'identification des localisations.
En entrée nous avons un tableau de trois colonnes qui a été créer au cours des étapes de preprocessing :
    • String (str) : qui est la string d'origine
    • Entite (list) : qui est l'entité qui a été identifiée dans la string d'origine, nettoyée et decoupée en liste de mots.
    • String_list (list) : qui est la string d'origine nettoyée et decoupée en liste de mots.

En sortie nous avons un tableau de 3 colonnes car la colonne "String_list" est supprimée dans la dernière fonction :
    • String (str) : qui est la string d'origine
    • Entite (str) : qui est l'entité identifiée dans la string d'origine nettoyée et decoupée en liste de mots, qui a été retransformée en chaine de caractère.
    • Localisation (str) : qui sont les localisations trouvées soit dans la colonne "Entite" soit dans la colonne "String_list"

    exemple d'initialisation de la fonction qui les appelle toute :
        df, df['localisation'], df['entite'] = concat_geo_good_and_rattrapage(df, 'string_list', df['string_list'],df['entite'], dico_1, dico_2)
"""

from unidecode import unidecode
import re
import pandas as pd
import numpy as np
import nltk
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('words')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

english_vocab = set(w.lower() for w in nltk.corpus.words.words())

### Cette fonction permet d'enlever les accents.
### On utilise volontairement pas la fonction remove_accent qui ne marche pas
### sur certaine typologie de données contenue dans le dataframe

def transformation_unidecode(text):
  """Cette fonction utilise le module unidecode qui permet d'enlever les accents
    en remplaçant les caractères de chaines de texte par des caractères ASCII.
    On utilise isinstance() qui renvoie un booléen, car c'est une fonction
    qui prend en compte l'héritage (https://docs.python.org/fr/3/tutorial/classes.html)

    source documentation :
      • https://stackoverflow.com/questions/19771751/how-to-use-unidecode-in-python-3-3
      • https://docs.python.org/fr/3/howto/unicode.html
  """

  if isinstance(text, str):
    resultat = unidecode(text)
  else:
    resultat = text

  return resultat

### Cette fonction importe le csv "gr_locs_csv" qui permets de créer les dico_1 et dico_2 necessaires à nos fonctions de localisation

def import_locs(gr_locs_csv):
  """Cette fonction :
      - Importe le csv qui permet de faire les étapes de localisation.
        Il faut impérativement la faire fonctionner avec le fichier 'gr_locs.csv'

      - Reattribue le type string à la colonne 'name'

      - Transforme les chaines de caractère de la colonne 'name' en minuscule
        en leur retirant les accents de sorte que le texte soit comparable avec
        celui contenu dans les colonnes "String_list" et "Entite" qui ont été préformattées.

  Le df_locs nous permettra de construire les dictionnaires de détection de
  localisation nommé : dico_1 et dico_2"""

  df_locs = pd.read_csv(gr_locs_csv, low_memory=False) #Pour eviter une erreur sur les colonnes 11, 12 et 13

  df_locs['name'] = df_locs['name'].astype(str)

# Ici on enlève les accents grâce à la fonction "transformation_unidecode" car la fonction "remove accent" est moins performante sur ce dataset qui comporte des Nan
  df_locs.loc[:, 'name'] = df_locs['name'].apply(transformation_unidecode).str.lower()
  df_locs.loc[:, 'alternatenames'] = df_locs['alternatenames'].apply(transformation_unidecode).str.lower()

  return df_locs

### Cette fonction permet de filtrer les localisations du csv qui ont 0 habitants

def population_filtre(df_locs, df_locs_population):
  """Fonction qui permet de filtrer des villes selon la taille de leur population
    dans le dataFrame df_locs. Pour se faire on récupère les indices des lignes
    concernées et on les drop. Le df_locs nous permettra de construire les dictionnaires de
    détection de localisation nommé : dico_1 et dico_2"""

  indice_ligne_geoloc_nulle = []
  filtre_numerique = 0

  for k in range(len(df_locs_population)):
    if df_locs_population[k] <= filtre_numerique :   #Filtre ici
      indice_ligne_geoloc_nulle.append(k)

  df_locs.drop(indice_ligne_geoloc_nulle, axis = 0, inplace=True)

## Etape 2 - Les index de ligne ne sont plus les bons puisque l'on a supprimés des lignes.
## On va donc reset les index avec un inplace = True

  df_locs.reset_index(drop = True, inplace = True) # modifie le dataframe directement.

  return df_locs

### Ces deux fonctions permettent de créer le DataFrame df_locs_names dont on se servira à présent.
### Ce dernier est composé dans sa colonne "name" des noms de localisations
### du csv colonne "name" et colonne "alternatenames"

def create_locs_names(df_locs_column_name):
  """Cette fonction permet la création du dataFrame 'df_locs_names'
     qui contient 3 colonnes et dont la colonne terms contient des listes
     d'un à plusieurs mots seloncla longueur du nom de la localisation
     contenu dans la colone 'name'."""

  data_loc = [[loc, loc.lower().split(), len(loc.split())] for loc in df_locs_column_name]
  columns_loc = ['name','terms', 'length']
  df_localisation_names = pd.DataFrame(data_loc, columns = columns_loc)

  return df_localisation_names

def create_locs_alternatenames(df_locs_column_alternatenames):
  """Cette fonction permet """

  data_loc_alternatenames = []
  columns_loc = ['name','terms', 'length']

  df_locs_column_alternatenames = df_locs_column_alternatenames.astype(str)

  for k in range(len(df_locs_column_alternatenames)):
    if df_locs_column_alternatenames[k] != "nan":
      liste_localisation_unique = df_locs_column_alternatenames[k].split(",")

      for loc in liste_localisation_unique:
        data_loc_alternatenames.append([loc, loc.lower().split(), len(loc.split())])

  df_localisation_alternatenames = pd.DataFrame(data_loc_alternatenames, columns = columns_loc)

  return df_localisation_alternatenames

def df_locs_names_complet(df_locs_column_name, df_locs_column_alternatenames):
  """Cette fonction créé le df_loc_names avec une fusion des résultats
  sous forme de dataframe des fonction 'create_locs_names' et 'create_locs_alternatenames' """

## Etape 1 - Générer le df_localisation_names
  df_localisation_names = create_locs_names(df_locs_column_name)

## Etape 2 - Générer le df_localisation_alternatenames
  df_localisation_alternatenames = create_locs_alternatenames(df_locs_column_alternatenames)

## Etape 3 - Fusionner les deux dataframe
  df_locs_fusion = pd.concat([df_localisation_names, df_localisation_alternatenames], ignore_index=True)

## Etape 4 - Supprimer les doublons (car les même noms de localisation étaient parfois présents dans les deux colonnes)
  df_locs_names_complet = df_locs_fusion.drop_duplicates(subset=['name'])

  return df_locs_names_complet

### Les trois fonctions ci-dessous permettent de créer les dictionnaires "dico_1" et "dico_2" en initialisant la fonction "create_dico_one_two"

def dico_2(column_name_minuscule):
  """Cette fonction crée le dictionnaire de forme 2 qui utilise
     l’indice des lignes du dataframe en clé. Pour le générer il prend en entrée
     une panda série dont les valeurs sont en minuscules grâce à la fonction 'lower_columns'
     et donne en output un dictionnaire formé par le zip de deux
     listes :
     • liste_cle : correspond aux index des lignes du dataframe et donnera
                   les clés du dico_2
     • liste_valeur : correspond aux valeurs contenues dans les lignes du dataframe
                      et donnera les valeurs associées aux clés du dico_2"""

  liste_cle = column_name_minuscule.index.tolist()
  liste_valeur = column_name_minuscule.tolist()

  return dict(zip(liste_cle, liste_valeur))


def dico_1(column_terms):
  """Cette fonction créé le dictionnaire de forme 1 ({'new' : [0, 1]})
     qui utilise chaque élément de la liste contenue dans chaque ligne
     du DataFrame comme clé, et prend en valeur l'indice de la ligne où
     cet élément est présent."""

  dictionnaire = {}
  serie_to_df = column_terms.to_frame()

  for index, row in serie_to_df.iterrows():
    for element in row[0]:
      dictionnaire.setdefault(element,[]).append(index)

  return dictionnaire

def create_dico_one_two(column_name_minuscule,column_terms):
  """Cette fonction permet de créer simultanément les deux dictionnaires codés
  dans la fonction dico_1 et dico_2"""

  # Création des deux dictionnaires
  dico_one = dico_1(column_terms)
  dico_two = dico_2(column_name_minuscule)

  return dico_one,dico_two

### Les foctions ci-dessous permettent de trouver les localisations

def reperage_localisation_niveau_1(df_entite_column, dico_1, dico_2):
    """
    Prend en input la série df_coi['clean_terms'], le dico_1 et le dico_2
    Finalité est de créer une liste nommée "is_loc" qui va servir à créer
    la future colonne 'islocalisation' du dataframe df_coi.

    {
        "new": [...indexes]
    }
    """

    serie_is_localisation = []
    serie_is_element_geo = []
    wrong_localisation = ["center", "centre", "centro", "central", "street", "road",
                          "avenue", "boulevard", "place", "federal", "way", "city",
                          "state", "province", "region", "regional", "park",
                          "national", "council", "international", "building",
                          "departamento", "department", "department",
                          "world", "rural", "earth"]

    localisation_word = set(dico_1.keys())

    for clean_terms in df_entite_column:
        indices_can_be_loc_terms = []
        is_loc_row = [False] * len(clean_terms)
        elemnt_geo_row = []

        for term in clean_terms:
            if term in localisation_word:
                indices_can_be_loc_terms.append(dico_1[term])
            else:
                indices_can_be_loc_terms.append([])

        if indices_can_be_loc_terms == [[] for i in range(len(indices_can_be_loc_terms))]:
            serie_is_element_geo.append(elemnt_geo_row)
            serie_is_localisation.append(is_loc_row)
            continue

        can_be_loc_term_index = 0
        while can_be_loc_term_index < len(clean_terms):
            while can_be_loc_term_index < len(clean_terms) and indices_can_be_loc_terms[can_be_loc_term_index] == []:
                can_be_loc_term_index += 1

            if can_be_loc_term_index == len(clean_terms):
              break
            # At this state our current can_be_loc_term_index corresponds
            # to an index that might be in a localisation word
            localisation_indices = indices_can_be_loc_terms[can_be_loc_term_index]
            localisations = [dico_2[ind] for ind in localisation_indices]
            localisations_split = [loc.split(" ") for loc in localisations]
            matching = False
            matching_length = 0
            match_loc = ""
            for loc in localisations_split:
                term_index = loc.index(clean_terms[can_be_loc_term_index])

                best_loc_length = min(len(clean_terms) - can_be_loc_term_index, len(loc) - term_index)
                for i in range(best_loc_length, 0, -1):
                    if loc[term_index:term_index+i] == clean_terms[can_be_loc_term_index:can_be_loc_term_index+i]:
                        if i > matching_length and not match_loc in wrong_localisation:
                            matching = True
                            match_loc = " ".join(loc[term_index:term_index+i])
                            matching_length = i
                        break

            if not matching:
                can_be_loc_term_index += 1
            else:
                if not match_loc in wrong_localisation:
                    elemnt_geo_row.append(match_loc)
                    for k in range(matching_length):
                        is_loc_row[can_be_loc_term_index + k] = True
                    can_be_loc_term_index += matching_length
                else:
                    can_be_loc_term_index += 1
        serie_is_element_geo.append(elemnt_geo_row)
        serie_is_localisation.append(is_loc_row)

    return (serie_is_localisation, serie_is_element_geo)

def reperage_localisation_niveau_2(col_is_element_geo, col_string_list, col_entite, dico_1, dico_2):
  """Cette fonction vérifie que quand il n'y a pas de localisation dans
  la cellule de la colonne 'is_element_geo' ([]) c'est bien par ce qu'il n'y
  en a pas dans la colonne 'clean_coi_terms'. Pour ce faire, elle utilise
  le principe de la fenètre glissante pour vérifier la présence d'une
  localisation à i+5 ou i-5 de la position de l'organisation (mentionnée
  dans la colonne 'clean_terms'). Si c'est le cas, elle stock le résultat
  dans une variable "bag_of_words". Puis il faudra appeler la fonction géo_V4
  sur le résultat de celle-ci pour ratraper cette localisation dans un tupple classique

  ATTENTION : Au delà de 5 mots la fenêtre est trop stricte et laisse passer des organisation correctes par la suite"""


  words_to_test = []

  for k in range(len(col_is_element_geo)):
    i = 0
    found_indice_org = False  # Ici on souhaite obtenir l'indice du premier terme d’organisation
    term = col_is_element_geo[k]  # [] ou ['republic korea']
    count_not_found_indices = 0
    count_already_loc = 0

    if term == [] :
      while i < (len(col_string_list[k]) - len(col_entite[k])) and not found_indice_org:
        if col_string_list[k][i : (i + len(col_entite[k]))] == col_entite[k]:

          found_indice_org = True
          bag_of_words = col_string_list[k][max(i - 5 , 0) : min((i + len(col_entite[k]) + 5), len(col_string_list[k]))]

        for i in range(len(col_string_list[k])-1):
          if col_string_list[k][i:i+2] == col_entite[k]:
            found_indice_org = True
            bag_of_words = col_string_list[k][max(i - 5 , 0) : min((i + len(col_entite[k]) + 5), len(col_string_list[k]))]

        i+=1

      if not found_indice_org:
        bag_of_words = []
        count_not_found_indices+=1

      words_to_test.append(bag_of_words)

    else:
      words_to_test.append([])
      count_already_loc+=1


  return words_to_test

def concat_geo_good_and_rattrapage(df, col_to_delite, col_string_list, col_entite, dico_1, dico_2):
  """Cette fonction permet la création d'une colonne 'element_geo_final'
     qui spécifie tous les éléments de localisations trouvé sur la ligne en question.
     Cette colonne est le résultat de la concaténation de la colonne 'element_geo_rattrapage'
     (résultat de la fonction de rattrapage géo) avec la colonne 'element_geo_good'
    (résultat de la première fonction de localisation)"""


  element_geo_final = []
  chaine_element_geo_final = []
  entite_string = []

  # Etape 1 - reperage_localisation_niveau_1

  serie_is_loc, serie_elmnt_geo = reperage_localisation_niveau_1(col_entite, dico_1, dico_2)
  col_is_element_geo = serie_elmnt_geo

  # Etape 2 - reperage_localisation_niveau_2 (col_is_element_geo, col_string_list, col_entite, dico_1, dico_2)

  col_words_to_test = reperage_localisation_niveau_2(col_is_element_geo, col_string_list, col_entite, dico_1, dico_2)

  # Etape 3 - il faut à nouveau passer la fonction de l'étape 1 sur le resultat de la fonction 2
  serie_is_loc_2, serie_elmnt_geo_2 = reperage_localisation_niveau_1(col_words_to_test, dico_1, dico_2)
  is_element_geo_rattrapage = serie_elmnt_geo_2

# Etape de concaténation des deux résultats

  for k in range(len(is_element_geo_rattrapage)):
    if is_element_geo_rattrapage[k] == []:
      element_geo_final.append(col_is_element_geo[k])
    else:
      element_geo_final.append(is_element_geo_rattrapage[k])

    chaine_element_geo_final.append(" ".join(element_geo_final[k]))

  for l in range(len(col_entite)):
    entite_string.append(" ".join(col_entite[l]))

  df = df.drop(columns=col_to_delite)

  return df, chaine_element_geo_final, entite_string
