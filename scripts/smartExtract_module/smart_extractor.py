import re
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score
import string
import nltk
import json
from flask import request
from flask import jsonify, make_response
from flask import Flask
import os
import ssl
# try:
#    _create_unverified_https_context = ssl._create_unverified_context
# except AttributeError:
#    pass
# else:
#    ssl._create_default_https_context = _create_unverified_https_context

# nltk.download()
# nltk.download('punkt')
# nltk.download('stopwords')
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from joblib import dump, load
import pickle
import time

app = Flask(__name__)
port = 4001
models_and_vects = {}


@app.route("/", methods=["POST"])
def smartExtract():
    if request.is_json:
        req = request.get_json()
        response_body = process_json(req)
        res = make_response(response_body, 200)
        return res
    else:
        return make_response(jsonify({"message": "Request body must be JSON"}), 400)


def preprocess(sentence):
    sentence = sentence.lower()
    sentence = re.sub(r'\d+', '', sentence)
    sentence = sentence.translate(str.maketrans("", "", string.punctuation))
    stemmer = PorterStemmer()
    words = word_tokenize(sentence)
    words = [stemmer.stem(word) for word in words]
    sentence = ' '.join(words)
    return sentence


def to_matrix(df):
    tf_vectorizer = TfidfVectorizer(max_features=20000)
    tf_matrix = tf_vectorizer.fit_transform(df['preprocessed'])
    return tf_matrix, tf_vectorizer


def extract_decide_profile(df, profile):
    labels = [1 for i in range(len(df))]
    for i in range(len(df)):
        extract = 1
        for pr, nb in profile.items():
            if pr == "organization":
                try:
                    if len(df['extractedOrgs'][i]) < nb:
                        extract = 0
                except:
                    extract = 0
            elif pr == "location":
                try:
                    if len(df['extractedLocs'][i]) < nb:
                        extract = 0
                except:
                    extract = 0
            elif pr == "person":
                try:
                    if len(df['extracted_pers'][i]) < nb:
                        extract = 0
                except:
                    extract = 0
        labels[i] = extract
    df["extract"] = labels
    df.drop(['extractedOrgs', 'extractedLocs', 'extractedPers'], axis=1)
    if len(df) < 1000:  # We can't decide before having seen enough data
        whatToDo = "model"
    elif df["extract"].mean() <= 0.05:
        whatToDo = "none"
    else:
        whatToDo = "model"
    return df, whatToDo


def train_model(df_ftc, profile):
    df_from_load = load_df(profile)
    # if os.path.exists('./smartExtract_models/' + filename(profile) + '_model.joblib'):
    global models_and_vects
    # print(models_and_vects.keys())
    if filename(profile) in models_and_vects:
        # print("profile found")
        # model = load('./smartExtract_models/' + filename(profile) + '_model.joblib')
        model = models_and_vects[filename(profile)]['model']
        # print("model loaded")
        # tf_vect = pickle.load(open('./smartExtract_tf_vect/'+filename(profile) + "vectorizer.pickle", "rb"))
        tf_vect = models_and_vects[filename(profile)]['tf_vect']
        # print("tf_vect loaded")
        tf_matrix = tf_vect.transform(df_ftc['preprocessed'])
        y_true = df_ftc['extract']
        y_pred = model.predict(tf_matrix)
        f1 = f1_score(y_true, y_pred)
        print(f1)
        if f1 > 0.9:
            # print("modèle encore bon")
            train = False
            return df_from_load, train
    # t1 = time.time()
    df = pd.concat([df_ftc, df_from_load])
    # t2 = time.time()
    # print("temps de concaténation des dataframes ", t2-t1)
    # print("taille du dataset de train ", len(df))
    if len(df) < 1000:
        train = True
        # print("dataset trop petit")
        # tf_vect = TfidfVectorizer(max_features=20000)
        # tf_vect.fit(transform)
        # model = RandomForestClassifier()
    else:
        model = RandomForestClassifier()
        # print("training model")
        # t1 = time.time()
        tf_matrix, tf_vect = to_matrix(df)
        # t2 = time.time()
        # print("temps de vectorisation du dataset ", t2-t1)
        df_tfidf = pd.DataFrame.sparse.from_spmatrix(tf_matrix)
        df_tfidf.reset_index(inplace=True, drop=True)
        df.reset_index(inplace=True, drop=True)
        extract_tfidf = pd.concat([df_tfidf, df['extract']], axis=1)
        # extract_tfidf = df_tfidf.assign(extract=df['extract'])
        X_train, X_test = train_test_split(extract_tfidf, test_size=0.2)
        y_train = X_train['extract']
        y_test = X_test['extract']
        X_train = X_train.drop(['extract'], axis=1)
        X_test = X_test.drop(['extract'], axis=1)
        # t3= time.time()
        # print("temps de préparation des données) ",t3-t2)
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        y_true = y_test
        f1 = f1_score(y_true, y_pred)
        print("F1 SCORE ", f1)
        model.fit(X_test, y_test)
        # t4 = time.time()
        # print("temps d'entrainement des données ", t4-t3)
        train = True
        if f1 > 0.9:
            models_and_vects[filename(profile)] = {}
            train = False
            # print("modèle entrainé")
            # save_model(model, profile)
            models_and_vects[filename(profile)]['model'] = model
            # print("model saved")
            # save_tf_vect(tf_vect,profile)
            models_and_vects[filename(profile)]['tf_vect'] = tf_vect
            # print("tf_vect saved")
    return df, train


def filename(profile):
    filename = ''
    if 'organization' in profile.keys():
        filename += 'organization' + str(profile['organization'])
    if 'location' in profile.keys():
        filename += 'location' + str(profile['location'])
    if 'person' in profile.keys():
        filename += 'person' + str(profile['person'])
    return filename


# def save_model(model, profile):
#    parent_dir = './'
#    dir = 'smartExtract_models'
#    os.makedirs(os.path.join(parent_dir, dir), exist_ok=True)
#    dump(model,'./smartExtract_models/'+filename(profile) + '_model.joblib')
#    print("model saved")
#    return None

# def save_tf_vect(tf_vect,profile):
#    parent_dir = './'
#    dir = 'smartExtract_tf_vect'
#    os.makedirs(os.path.join(parent_dir, dir), exist_ok=True)
#    pickle.dump(tf_vect, open('./smartExtract_tf_vect/'+filename(profile) + "vectorizer.pickle", "wb"))
#    return None

def save_df(df, profile):
    parent_dir = './'
    dir = 'smartExtract_dataframes'
    os.makedirs(os.path.join(parent_dir, dir), exist_ok=True)
    path = "./smartExtract_dataframes/" + filename(profile) + ".csv"
    df.to_csv(path, index=False)
    return None


def load_df(profile):
    if os.path.exists("./smartExtract_dataframes/" + filename(profile) + ".csv"):
        df = pd.read_csv("./smartExtract_dataframes/" + filename(profile) + ".csv")
    else:
        df = pd.DataFrame()
    return df


def process_json(json_dict):
    if "extractedData" in json_dict.keys():
        data_list = json_dict["extractedData"]
        df_from_json = pd.DataFrame(data_list)
        output_list = []
        for context_id in df_from_json['contextID'].unique():
            df_ftc = df_from_json.loc[df_from_json['contextID'] == context_id]
            df_ftc['preprocessed'] = df_ftc['stringValue'].apply(lambda x: preprocess(x))
            profile = df_ftc['profile'].iloc[0]
            df_ftc.drop(['profile', "contextID"], axis=1)
            df_ftc, what_todo = extract_decide_profile(df_ftc, profile)
            if what_todo == "model":
                df, train = train_model(df_ftc, profile)
                save_df(df, profile)
                if train:
                    output_list.append({'contextID': int(context_id), "newState": "extract"})
                else:
                    output_list.append({'contextID': int(context_id), "newState": "model"})
            else:
                output_list.append({'contextID': int(context_id), "newState": what_todo})
        # print(output_list)
        return json.dumps(output_list)
    else:
        global models_and_vects
        profile = json_dict["profile"]
        node_id = json_dict["nodeID"]
        # t1 = time.time()
        line = pd.DataFrame({'nodeID': int(node_id), 'stringValue': json_dict['stringValue'],
                             'preprocessed': preprocess(json_dict['stringValue'])}, index=[0])
        # t2 = time.time()
        # print("temps de preprocessing ", t2-t1)
        # model = load('./smartExtract_models/' + filename(profile) + '_model.joblib')
        model = models_and_vects[filename(profile)]['model']
        # t3 = time.time()
        # print("temps de récupération du modèle ", t3-t2)
        # tf_vect = pickle.load(open('./smartExtract_tf_vect/'+filename(profile) + "vectorizer.pickle", "rb"))
        tf_vect = models_and_vects[filename(profile)]['tf_vect']
        # t4 =time.time()
        # print("model loaded")
        # print("temps de récupération du tfidf ", t4-t3)
        tf_idf_line = tf_vect.transform(line['preprocessed'])
        # print("tf_vect loaded")
        # t5 = time.time()
        # print("temps de vectorisation ", t5-t4)
        extract = model.predict(tf_idf_line)
        # t6 = time.time()
        # print("temps de prédiction ", t6-t5)
        output_dict = {'nodeID': int(node_id), 'extract': int(extract)}
        # t7 = time.time()
        # print("temps total pour le traitement du noeud ", t7-t1)
        # print(output_dict)
        return json.dumps(output_dict)


if __name__ == "__main__":
    app.run(port=port,
            debug=False)
